﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace CheckStore
{
  class EmailSender
  {
    private const int SMTP_PORT = 587;
    private const string SMTP_SERVER = "smtp.server.com";
    private const string EMAIL_ADDRESS = "your@email.com";
    private const string EMAIL_PASSWORD = "yourPassW0rd";

    public static void SendEmail(string subject, string content, bool isHtml)
    {
      MailMessage mail = new MailMessage(EMAIL_ADDRESS, EMAIL_ADDRESS);
      SmtpClient client = new SmtpClient(SMTP_SERVER, SMTP_PORT)
      {
        Port = SMTP_PORT,
        DeliveryMethod = SmtpDeliveryMethod.Network,
        UseDefaultCredentials = false,
        EnableSsl = true,
        Credentials = new NetworkCredential(EMAIL_ADDRESS, EMAIL_PASSWORD)
      };
      mail.Subject = subject;
      mail.Body = content;
      mail.IsBodyHtml = isHtml;
      try
      {
        client.Send(mail);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Failed to send email from " + EMAIL_ADDRESS + " to " + EMAIL_ADDRESS + ".", ex);
      }
    }
  }
}
