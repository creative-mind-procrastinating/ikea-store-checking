﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Xml;
using System.Xml.Linq;

namespace CheckStore
{
  class Program
  {
    static async System.Threading.Tasks.Task Main(string[] args)
    {
      var hostBuilder = new HostBuilder()
                .ConfigureServices((hostContext, services) =>
                {
                  services.AddHostedService<WorkerHostedService>();
                });

      await hostBuilder.StartAsync();
    }
  }
}
