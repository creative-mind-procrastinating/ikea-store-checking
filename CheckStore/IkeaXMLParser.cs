﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace CheckStore
{
  class IkeaXMLParser
  {
    #region Fields
    //example of the XML at https://www.ikea.com/cz/cs/iows/catalog/availability/10244516/
    private const string localStoreTag = "localStore";
    private const string buCodeTag = "buCode";
    private const string stockTag = "stock";
    private const string availableStockTab = "availableStock";
    #endregion Fields

    #region Properties
    public XmlDocument IkeaXML { get; set; }
    #endregion Properties

    public IkeaXMLParser(XmlDocument document)
    {
      IkeaXML = document;
    }

    public int GetItemStockCount(string storeId)
    {
      var elements = IkeaXML.GetElementsByTagName(localStoreTag);
      int result = 0;

      foreach (var element in elements)
      {
        XmlNode localStore = element as XmlNode;
        var store = localStore.Attributes.GetNamedItem(buCodeTag).Value;


        if (store == storeId)
        {
          var stock = localStore.SelectSingleNode(stockTag);
          var availableStock = stock.SelectSingleNode(availableStockTab).InnerText;

          int.TryParse(availableStock, out result);
          break;
        }
      }

      return result;
    }
  }
}
