﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace CheckStore
{
  class WorkerHostedService : BackgroundService
  {

    private static string url = "https://www.ikea.com/cz/cs/iows/catalog/availability/70423992/";
    //Ostrava
    private static string store = "278";
    private static int hours = 12;

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
      while (!stoppingToken.IsCancellationRequested)
      {
        Process();
        await Task.Delay(TimeSpan.FromHours(hours), stoppingToken);
      }
    }

    private void Process()
    {
      HttpClient http = new HttpClient();
      var data = http.GetAsync(url).Result.Content.ReadAsStringAsync().Result;

      XmlDocument xmlDoc = new XmlDocument();
      xmlDoc.LoadXml(data);

      IkeaXMLParser ikeaXMLParser = new IkeaXMLParser(xmlDoc);
      int count = ikeaXMLParser.GetItemStockCount(store);

      if (count > 0) EmailSender.SendEmail("Ikea talíře", "Je skladem " + count + " kusů", true);
    }
  }
}
